package com.bsc.repositories;

import com.bsc.bo.PackageBO;

import java.util.*;
import java.util.stream.Collectors;

// simulate repository as a mean of data persistence
// only in memory, but usually I would persist on DB with this
// using JPA and some implementation, like Hibernate...
public final class PackageRepo {

    private Map<String, PackageBO> packages;
    private static PackageRepo instance;

    private PackageRepo() {
        packages = new HashMap();
    }

    public static synchronized PackageRepo getInstance() {
        if(instance == null) {
            instance = new PackageRepo();
        }
        return instance;
    }

    // I am sure there is a sexy function for this in Kotlin. :-)
    // something with .map{ it.value }.sortBy{ it.weight }
    public List<PackageBO> getPackagesSorted() {
        // this is horrible @_@ long live Kotlin
        return packages.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    public Map<String, PackageBO> getPackages() {
        return packages;
    }

    public void setPackages(Map<String, PackageBO> packages) {
        this.packages = packages;
    }
}
