package com.bsc.repositories;

import com.bsc.bo.FeeBO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FeeRepo {

    private List<FeeBO> fees;
    private static FeeRepo instance;

    private FeeRepo() {
        fees = new ArrayList();
    }

    public static synchronized FeeRepo getInstance() {
        if(instance == null) {
            instance = new FeeRepo();
        }
        return instance;
    }

    public List<FeeBO> getFees() {
        return fees;
    }

    public void setFees(List<FeeBO> fees) {
        this.fees = fees;
    }

    // post-processing methods like these
    // should be in my opinion in standalone services
    // in this case, such service is just a piece of unnecessary code
    // and also it depends on the preference
    // I encountered opinion that every repo must have a service in service layer
    // and that service can only call to its repo
    // and on the contrary, I encountered also the opposite opinion
    // that services can call whatever repos they might need from repo layer
    public void orderFees() {
        Collections.sort(fees);
    }
}
