package com.bsc.services;

import com.bsc.bo.FeeBO;
import com.bsc.bo.PackageBO;
import com.bsc.repositories.FeeRepo;
import com.bsc.repositories.PackageRepo;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

// concrete service classes can't be inherited
public final class OutputService {
    // frequency is in TimeUnit.SECONDS
    // In spring I would introduce application.property with value 60,
    // and my own application-mr.property with value 3
    // and setup configuration to use my active profile as 'mr'
//    private int outputFrequency = 60;
    private int outputFrequency = 15;
    private ScheduledExecutorService scheduler;

    // implementation of the singleton pattern
    // usually I let Spring do this for me
    private static OutputService instance;
    private static PackageRepo packageRepo;
    private static FeeRepo feeRepo;

    private OutputService(PackageRepo packageRepo, FeeRepo feeRepo) {
        OutputService.packageRepo = packageRepo;
        OutputService.feeRepo = feeRepo;
        scheduler = Executors.newScheduledThreadPool(1);
        // this is a "disappointment" for me - I was looking forward to do some thread programming
        // with concurrent data, object locking and stuff
        // but this thing does everything on it's own, and works surprisingly good...
        scheduler.scheduleAtFixedRate(writeData(), outputFrequency, outputFrequency, TimeUnit.SECONDS);
    }

    // thread safe, lazy init singleton
    public static synchronized OutputService getInstance(PackageRepo packageRepo, FeeRepo feeRepo) {
        if(instance == null) {
            instance = new OutputService(packageRepo, feeRepo);
        }
        return instance;
    }

    private static synchronized Runnable writeData() {
        return () -> {
            System.out.println("OUTPUT OF CURRENTLY STORED DATA:");
            List<PackageBO> data = packageRepo.getPackagesSorted();
            if (!feeRepo.getFees().isEmpty()) {
                computeFees(data, feeRepo.getFees());
                data.forEach(single ->
                        System.out.println(
                            single.getPostalCode()
                            + " "
                            + single.getWeightString()
                            + " "
                            + single.getFeeString()));
                System.out.println();
            } else {
                data.forEach(single ->
                        System.out.println(
                                single.getPostalCode()
                                + " "
                                + single.getWeightString()));
                System.out.println();
            }
        };
    }

    // I will use Java mechanic here
    // Java always gives method params by pointer reference
    // hence, I can update fees on the parameter data
    // and after the method, I will have them updated at the place of this method call
    // hence no need to specify return type and return like a new collection of data
    private static void computeFees(List<PackageBO> data, List<FeeBO> fees) {
        // as I mentioned in FeeBO
        // I will iterate linearly trough PackageBO data
        // and apply best-fit for each input by again linearly iterate trough the FeeBOs
        // it is not the best approach, but for this amount of data will do
        // for big data, I could introduce binary search to fasten my best-fit variation
        for (PackageBO onePackage : data) {
            FeeBO maxWeightFee = fees.get(0);
            if (onePackage.getWeight() >= maxWeightFee.getWeight()) {
                onePackage.setFee(maxWeightFee.getFee());
                continue;
            }
            FeeBO minWeightFee = fees.get(fees.size() - 1);
            if (onePackage.getWeight() <= minWeightFee.getWeight()) {
                onePackage.setFee(minWeightFee.getFee());
                continue;
            }
            // these were the boundary intervals (either greater or equal the max amount, or lower or equal the min amount)
            // for everything in between, we will iterate and apply best-fit scenario
            // please note, that our FeeBOs are ordered in DESCENDING order by weight
            for (int i = 1; i < fees.size(); i++) {
                FeeBO maxWeight = fees.get(i - 1);
                FeeBO minWeight = fees.get(i);
                double packageWeight = onePackage.getWeight();
                if (packageWeight < maxWeight.getWeight() && packageWeight >= minWeight.getWeight()) {
                    // as instructed in the assignment
                    // we will apply the fee from the min spectrum of the interval
                    onePackage.setFee(minWeight.getFee());
                    break; // we can terminate the linear iteration, since we have our result
                }
            }
        }
    }

    public ScheduledExecutorService getScheduler() {
        return scheduler;
    }
}
