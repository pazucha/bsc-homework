package com.bsc.services;

import com.bsc.bo.PackageBO;
import com.bsc.exceptions.InputDataValidationException;
import com.bsc.repositories.PackageRepo;

import java.util.Map;
import java.util.Scanner;

// concrete service classes can't be inherited
public final class UserInputService extends InputService {
    // const string comparison instead of word, in case of change easy update on one place
    private final String QUIT = "quit";

    // implementation of the singleton pattern
    // usually I let Spring do this for me
    private static UserInputService instance;
    private static PackageRepo repo;
    private static OutputService outService;

    private String line;
    private Scanner inRead;

    // this is basically autowiring in Spring, just done manually
    private UserInputService(PackageRepo repo, OutputService outService) {
        UserInputService.repo = repo;
        UserInputService.outService = outService;
        // I could initialize in the var section too
        inRead = new Scanner(System.in);
    }

    // thread safe, lazy init singleton
    public static synchronized UserInputService getInstance(PackageRepo repo, OutputService outService) {
        if(instance == null) {
            instance = new UserInputService(repo, outService);
        }
        return instance;
    }

    public void readInput() {
        line = inRead.nextLine();
        while (!line.equals(QUIT)) {
            // console input is always line 1
            // this could be interesting should we want to input more than one line commands
            int i = 1;
            // I don't like to use var in Java, since it's strongly typed and procedural
            // I like to use it in Kotlin, which is more functional oriented and hence more non-procedural oriented
            //I use it here to showcase I know about it
            Map<String, PackageBO> storedData = repo.getPackages();

            handleInputData(line, storedData, i);

            // persist the data
            repo.setPackages(storedData);
            line = inRead.nextLine();
        }

        // TODO: not sure if this is save or anything, can't think of any reason why not
        // but I will take look tomorrow if something will come up in mind
        outService.getScheduler().shutdown();
    }
}
