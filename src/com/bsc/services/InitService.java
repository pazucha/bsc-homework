package com.bsc.services;

import com.bsc.bo.FeeBO;
import com.bsc.bo.PackageBO;
import com.bsc.repositories.FeeRepo;
import com.bsc.repositories.PackageRepo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public final class InitService extends InputService {

    private static InitService instance;
    private static PackageRepo packageRepo;
    private static FeeRepo feeRepo;
    private File inputData;
    private File inputFees;

    private InitService(String[] args, PackageRepo packageRepo, FeeRepo feeRepo) {
        InitService.packageRepo = packageRepo;
        InitService.feeRepo = feeRepo;
        // this might be better as a standalone method outside constructor
        validateArgs(args);
        try {
            loadInitialData();
            if (inputFees != null && inputFees.exists()) {
                loadInitialFees();
            }
        } catch (FileNotFoundException e) {
            System.err.println("Fatal error: " + e.getMessage());
        }
    }

    public static synchronized InitService getInstance(String[] args, PackageRepo packageRepo, FeeRepo feeRepo) {
        if(instance == null) {
            instance = new InitService(args, packageRepo, feeRepo);
        }
        return instance;
    }

    private void loadInitialFees() throws FileNotFoundException {
        Scanner read = new Scanner(inputFees);
        List<FeeBO> initFees = new ArrayList();
        int i = 1;
        while (read.hasNextLine()) {
            String line = read.nextLine().trim();
            handleInputData(line, initFees, i);
            i++;
        }

        feeRepo.setFees(initFees);
        // we need this so our best-fit method works properly
        // otherwise we might have non-ordered data
        // and that would cause inconsistencies during our compute fee phase
        // also we can't rely for users to provide ordered data
        feeRepo.orderFees();
    }

    private void loadInitialData() throws FileNotFoundException {
        Scanner read = new Scanner(inputData);
        Map<String, PackageBO> initData = new HashMap();
        int i = 1;
        while (read.hasNextLine()) {
            // trim is always a good idea, people like to leave whitespaces on random spots
            String line = read.nextLine().trim();
            handleInputData(line, initData, i);
            i++;
        }

        packageRepo.setPackages(initData);
    }

    private void validateArgs(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Not enough runtime arguments presented");
        }
        if (args.length > 2) {
            throw new IllegalArgumentException("Too many runtime arguments presented");
        }

        String path = System.getProperty("user.dir");
        inputData = new File(path + "\\" + args[0]);
        if(!inputData.exists()) {
            throw new IllegalArgumentException("File " + inputData.getName() + " does not exist");
        }

        if (args.length > 1) {
            inputFees = new File(path + "\\" + args[1]);
            if (!inputData.exists()) {
                throw new IllegalArgumentException("File " + inputFees.getName() + " does not exist");
            }
        }
    }
}
