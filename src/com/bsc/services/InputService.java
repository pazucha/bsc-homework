package com.bsc.services;

import com.bsc.bo.FeeBO;
import com.bsc.bo.PackageBO;
import com.bsc.exceptions.InputDataValidationException;
import com.bsc.repositories.FeeRepo;

import java.util.List;
import java.util.Map;

public abstract class InputService {

    protected void handleInputData(String line, Map<String, PackageBO> data, int i) {
        try {
            validateInputLine(line);
            PackageBO inputPackage = processInputLine(line);
            data.computeIfPresent(
                    inputPackage.getPostalCode(),
                    (postalCode, storedPackage) ->
                            new PackageBO(postalCode, storedPackage.getWeight() + inputPackage.getWeight()));
            data.putIfAbsent(inputPackage.getPostalCode(), inputPackage);
        } catch (InputDataValidationException e) {
            logInputValidationException(i, line, e.getMessage());
        }
    }

    // I am using overriding of the handleInputData method
    protected void handleInputData(String line, List<FeeBO> data, int i) {
        try {
            validateFeeLine(line);
            data.add(processFeeLine(line));
        } catch (InputDataValidationException e) {
            logInputValidationException(i, line, e.getMessage());
        }
    }

    // Fee processing methods should be (at this juncture) present in the InputService, because only that service will use them
    // I include them here, because I'd believe customers would like to change their fee data from manual input as well
    // then, it would have to be moved into this abstract service, so it can be used in the UserInputService too
    private void validateFeeLine(String line) {
        generalParamValidation(line);
        String[] params = line.split(" ");
        generalNumberValidation(params[0]);
        weightValidation(params[0]);
        generalNumberValidation(params[1]);
    }

    // I realize I could decompose these 2 methods further into a one more private method
    // e.g. validateLine, but I believe such decomposition would only serve to confuse a future programmer
    // so I leave the validation methods whole, so we can see in plain sight what sorts of validation must be done
    // for each line type (in this case input data or input fee)

    private void validateInputLine(String line) {
        generalParamValidation(line);
        String[] params = line.split(" ");
        generalNumberValidation(params[0]);
        weightValidation(params[0]);
        validatePostalCode(params[1]);
    }

    private void generalParamValidation(String line) {
        if (!line.contains(" ")) {
            throw new InputDataValidationException("Insufficient input parameters on this line");
        }
        if (line.split(" ").length > 2) {
            throw new InputDataValidationException("Too many input parameters on this line");
        }
    }

    private void weightValidation(String numberStr) {
        if (Double.parseDouble(numberStr) == 0) {
            throw new InputDataValidationException("Weight number must be greater than 0");
        }
    }

    private void validatePostalCode(String code) {
        // we can include more validation for postal codes inside this method
        if (code.length() != 5) {
            throw new InputDataValidationException("Illegal postal code input parameter - incorrect length");
        }
    }

    private void generalNumberValidation(String numberStr) {
        if (numberStr.charAt(0) == '-') {
            throw new InputDataValidationException("Negative weight nor fee is not allowed in this Universe");
        }
        if (numberStr.contains(",")) {
            throw new InputDataValidationException("Illegal decimal delimiter used. Please use dot instead of comma");
        }
        try {
            Double.parseDouble(numberStr);
        } catch (NumberFormatException e) {
            throw new InputDataValidationException("Unable to parse input weight number");
        }
    }

    private FeeBO processFeeLine(String line) {
        double weight = Double.parseDouble(line.substring(0, line.lastIndexOf(' ')));
        double fee = Double.parseDouble(line.substring(line.lastIndexOf(' ') + 1));
        return new FeeBO(weight, fee);
    }

    private PackageBO processInputLine(String line) {
        // I could use the String.split method here too, but I really like playing around with indexes
        // I would use split at a works assignment
        double weight = Double.parseDouble(line.substring(0, line.lastIndexOf(' ')));
        String postalCode = line.substring(line.lastIndexOf(' ') + 1);
        // we will leave fee as zero, and we will lazily compute it once the OutputService calls data to print
        return new PackageBO(postalCode, weight);
    }

    private void logInputValidationException(int i, String line, String message) {
        StringBuilder sb = new StringBuilder();
        sb.append("Error in ");
        // I believe in Kotlin we could use when clause here to distinguish between various class types
        // when clause in Kotlin is pretty epic
        if (this instanceof InitService) {
            sb.append("Input Data File ");
        } else if (this instanceof UserInputService) {
            sb.append("User Input Data ");
        }
        sb.append("- corrupted data on line ").append(i).append(": ").append(line);

        System.err.println(sb);
        System.err.println(message);
        System.err.println("Corrupted line will be skipped");
        System.err.println();
    }
}
