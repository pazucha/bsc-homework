package com.bsc.bo;

// I had an idea to make this BO as an interval unit
// and every BO would have min and max weight where the fee is applicable
// but I realized it's unnecessary work, when I tried to find a proper and fast algorithm
// to actually calculate said intervals
// so I decided to match the fee when OutputService wants to print data
// and for that I will use linear iteration trough saved FeeBOs and best-fit scenario
public class FeeBO implements Comparable<FeeBO> {
    private double weight;
    private double fee;

    public FeeBO(double weight, double fee) {
        this.weight = weight;
        this.fee = fee;
    }

    @Override
    public int compareTo(FeeBO o) {
        if (this.weight > o.weight) return -1;
        if (this.weight == o.weight) return 0;
        if (this.weight < o.weight) return 1;

        return 0;
    }

    public double getWeight() {
        return weight;
    }

    public double getFee() {
        return fee;
    }

    // no need for setters, these BOs will be filled with data upon creation
    // and that's it :-)
}
