package com.bsc.bo;

import java.text.DecimalFormat;

public class PackageBO implements Comparable<PackageBO> {
    private static final DecimalFormat dfW = new DecimalFormat("0.000");
    private static final DecimalFormat dfF = new DecimalFormat("0.00");

    private String postalCode;
    private double weight;
    private double fee;

    // we could use MapStruct to generate getters, setters and constructors

    public PackageBO(String postalCode, double weight) {
        this.postalCode = postalCode;
        this.weight = weight;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public double getWeight() {
        return weight;
    }

    public String getWeightString() {
        return dfW.format(weight);
    }

    public String getFeeString() {
        return dfF.format(fee);
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    // TODO: can I do this better with clean head?
    // TODO: do I need to override equals() and hashCode()?
    @Override
    public int compareTo(PackageBO o) {
        if (this.weight > o.weight) return -1;
        if (this.weight == o.weight) return 0;
        if (this.weight < o.weight) return 1;

        return 0;
    }
}
