package com.bsc;

import com.bsc.repositories.FeeRepo;
import com.bsc.repositories.PackageRepo;
import com.bsc.services.InitService;
import com.bsc.services.UserInputService;
import com.bsc.services.OutputService;

// MVC pattern is used to distribute operations
public class Main {

    public static void main(String[] args) {
        writeGreeting();

        PackageRepo packageRepo = PackageRepo.getInstance();
        FeeRepo feeRepo = FeeRepo.getInstance();

        // I don't need to inject InitService anywhere at this point
        InitService.getInstance(args, packageRepo, feeRepo);
        OutputService outService = OutputService.getInstance(packageRepo, feeRepo);
        UserInputService inService = UserInputService.getInstance(packageRepo, outService);


        inService.readInput();
    }

    private static void writeGreeting() {
        System.out.println("Hello. Enjoy this program.");
        System.out.println("This is a homework program for BSC.");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Run path is " + System.getProperty("user.dir"));
        System.out.println("Please provide all files mentioned in start arguments in this directory");
        System.out.println();
    }
}
