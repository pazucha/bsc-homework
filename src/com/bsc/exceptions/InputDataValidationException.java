package com.bsc.exceptions;

public class InputDataValidationException extends RuntimeException {
    public InputDataValidationException(String message) {
        super(message);
    }
}
